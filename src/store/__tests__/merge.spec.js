import createStore from '../store';

describe('GIVEN Jane is knowing John', function () {
  let facts;
  beforeEach(async () => {
    facts = createStore();
    await facts.merge({
      "@context": {
        "@vocab": "http://schema.org/"
      },
      "@type": "Person",
      "@id": "/person/42",
      "name": "Jane Doe",
      "knows": {
        "@id": "/person/43",
        "name": "John Doe"
      }
    })
  });

  describe('WHEN more people known to Jane a added to the store',
      () => {

        beforeEach(async () => {
          await facts.merge({
            "@context": {
              "@vocab": "http://schema.org/",
              "knows": {
                "@type": "@id"
              }
            },
            "@id": "/person/42",
            "knows": [
              "/friend/1",
              "/friend/2",
              "/friend/3"
            ]
          })
        });

        it('THEN the store contains all people known to Jane', () => {
          expect(facts.all()).toEqual({
            "@graph": [
              {
                "@id": "/person/42",
                "@type": "http://schema.org/Person",
                "http://schema.org/knows": [
                  {
                    "@id": "/person/43"
                  },
                  {
                    "@id": "/friend/1"
                  },
                  {
                    "@id": "/friend/2"
                  },
                  {
                    "@id": "/friend/3"
                  }
                ],
                "http://schema.org/name": "Jane Doe"
              },
              {
                "@id": "/person/43",
                "http://schema.org/name": "John Doe"
              }
            ]
          });
        });

      });

  describe(
      "WHEN another person's data does also include facts about people Jane knows",
      () => {

        beforeEach(async () => {
          await facts.merge({
            "@context": {
              "@vocab": "http://schema.org/"
            },
            "@id": "/person/44",
            "@type": "Person",
            "givenName": "Trudy",
            "knows": [
              {
                "@id": "/person/43",
                "givenName": "John"
              },
              {
                "@id": "/friend/of/trudy",
                "givenName": "Toni"
              }
            ]
          })
        });

        it('THEN the store contains all that data', () => {
          expect(facts.all()).toEqual({
            "@graph": [
              {
                "@id": "/friend/of/trudy",
                "http://schema.org/givenName": "Toni"
              },
              {
                "@id": "/person/42",
                "@type": "http://schema.org/Person",
                "http://schema.org/knows": {
                  "@id": "/person/43"
                },
                "http://schema.org/name": "Jane Doe"
              },
              {
                "@id": "/person/43",
                "http://schema.org/name": "John Doe",
                "http://schema.org/givenName": "John"
              },
              {
                "@id": "/person/44",
                "@type": "http://schema.org/Person",
                "http://schema.org/knows": [
                  {"@id": "/person/43"},
                  {"@id": "/friend/of/trudy"}
                ],
                "http://schema.org/givenName": "Trudy"
              }
            ]
          });
        });
      });
});

