import createStore from '../store';

describe('GIVEN an empty store', function () {
  let facts;
  beforeEach(async () => {
    facts = createStore();
  });

  describe('WHEN merging some resources in parallel',
      () => {
        beforeEach(async () => {
          facts.merge({
            "@context": {
              "@vocab": "http://schema.org/"
            },
            "@type": "Person",
            "@id": "/person/42",
            name: "Jane Doe"
          });
          facts.merge({
            "@context": {
              "@vocab": "http://schema.org/"
            },
            "@id": "/person/42",
            givenName: "Jane"
          });
          facts.merge({
            "@context": {
              "@vocab": "http://schema.org/"
            },
            "@id": "/person/42",
            familyName: "Doe"
          })
        });

        it('THEN all facts should be present eventually', async (done) => {
          setTimeout(() => {
            expect(facts.all()).toEqual({
              "@graph": [
                {
                  "@id": "/person/42",
                  "@type": "http://schema.org/Person",
                  "http://schema.org/name": "Jane Doe",
                  "http://schema.org/givenName": "Jane",
                  "http://schema.org/familyName": "Doe"
                }
              ]
            });
            done()
          })
        });

      });
});

