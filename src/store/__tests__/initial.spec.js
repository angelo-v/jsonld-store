import createStore from '../store';

const facts = createStore();

describe('initial store', () => {
    it('should be empty', () => {
        expect(facts.all()).toEqual({
            "@graph": []
        });
    });
});