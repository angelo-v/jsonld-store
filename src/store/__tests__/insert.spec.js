import createStore from '../store';

describe('fact insertions', () => {

  describe('insert to empty store', function () {
    let facts;
    beforeEach(() => {
      facts = createStore();
    });

    it('should insert facts', async () => {
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe"
      });
      expect(facts.all()).toEqual({
        "@graph": [
          {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe"
          }
        ]
      });
    });

    it('should insert multiple resources', async () => {
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe",
        address: {
          "@id": "/address/1337",
          addressLocality: "Capital City"
        }
      });
      expect(facts.all()).toEqual({
        "@graph": [
          {
            "@id": "/address/1337",
            "http://schema.org/addressLocality": "Capital City"
          },
          {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe",
            "http://schema.org/address": {
              "@id": "/address/1337"
            }
          }
        ]
      });
    });

  });

  describe('merging additional facts of same resource', () => {

    let facts;

    beforeAll(async () => {
      facts = createStore();
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe"
      });
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@id": "/person/42",
        givenName: "Jane"
      });
    });

    it('contains all facts in the final store', () => {
      expect(facts.all()).toEqual({
        "@graph": [
          {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe",
            "http://schema.org/givenName": "Jane"
          }
        ]
      });
    });
  });

  describe('merging additional facts of multiple existing resources', () => {

    let facts;

    beforeAll(async () => {
      facts = createStore();
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe",
        address: {
          "@id": "/address/1337",
          addressLocality: 'Capital City'
        }
      });
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@id": "/person/42",
        givenName: "Jane",
        address: {
          "@id": "/address/1337",
          addressLocality: 'Capital City',
          streetAddress: 'Main street'
        }
      });
    });

    it('contains all facts in the final store', () => {
      expect(facts.all()).toEqual({
        "@graph": [{
          "@id": "/address/1337",
          "http://schema.org/addressLocality": "Capital City",
          "http://schema.org/streetAddress": "Main street"
        }, {
          "@id": "/person/42",
          "@type": "http://schema.org/Person",
          "http://schema.org/address": {"@id": "/address/1337"},
          "http://schema.org/givenName": "Jane",
          "http://schema.org/name": "Jane Doe"
        }]
      });
    });
  });

  describe('merging facts of a second resource', () => {

    let facts;

    beforeAll(async () => {
      facts = createStore();
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/42",
        name: "Jane Doe"
      });
      await facts.merge({
        "@context": {
          "@vocab": "http://schema.org/"
        },
        "@type": "Person",
        "@id": "/person/43",
        name: "John Doe"
      });
    });

    it('contains all facts in the final store', () => {
      expect(facts.all()).toEqual({
        "@graph": [
          {
            "@id": "/person/42",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "Jane Doe"
          },
          {
            "@id": "/person/43",
            "@type": "http://schema.org/Person",
            "http://schema.org/name": "John Doe"
          }
        ]
      });
    });
  });
});