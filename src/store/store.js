import {compact, expand, flatten, frame} from 'jsonld';
import Freezer from 'freezer-js';

import Events from 'event-pubsub';

const forEachId = (facts, handler) => facts['@graph'].map(
    it => it["@id"]).forEach(handler);

async function notifySubscribers(newFacts, newGraph, oldGraph) {
  const flatFacts = await flatten(newFacts, this.context);
  forEachId(flatFacts, async updatedId =>
      this.events.emit(
          updatedId,
          await getResource(updatedId, newGraph, this.context),
          await getResource(updatedId, oldGraph, this.context)
      ));
}

async function getResource(id, graph, context) {
  return await compact(await frame(graph, {
    "@context": context,
    "@id": id
  }), context);
}

const mergeFacts = async function (previousFacts, facts, context) {
  const expandedPreviousFacts = await expand(previousFacts);
  const expandedNewFacts = await expand(facts);
  const concatted = [...expandedPreviousFacts, expandedNewFacts];
  return await flatten(concatted, context);
};

class Store {

  constructor(facts, context = {}) {
    this.freezer = new Freezer(facts);
    this.context = context;
    this.events = new Events;
  }

  all() {
    return this.freezer.get();
  }

  async merge(facts) {
    while (this.pendingMerge) {
      await this.pendingMerge;
    }
    const previousFacts = this.all();
    this.pendingMerge = mergeFacts(previousFacts, facts, this.context);
    const finalFacts = await this.pendingMerge;
    previousFacts['@graph'].reset(finalFacts["@graph"]);
    notifySubscribers.call(this, facts, finalFacts, previousFacts);
    delete this.pendingMerge;
    return this;
  }

  subscribe(id, handler) {
    this.events.on(id, handler);
  }

  unsubscribe(id, handler) {
    this.events.off(id, handler);
  }

  getResource(id, context = this.context) {
    return getResource(id, this.all(), context);
  }
}

export default (context) => new Store(context ? {
  "@context": context,
  "@graph": []
} : {
  "@graph": []
}, context);